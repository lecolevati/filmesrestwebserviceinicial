package controller.resources;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

public interface IFilmesResources {

	public Response buscarFilmes(@HeaderParam("token") long token);
	public Response buscarFilmesPorId(@PathParam("id") int id, @HeaderParam("token") long token);
	public Response buscarFilmesPorIdGenero(@PathParam("id") int id, @PathParam("codGen") int codGen, @HeaderParam("token") long token);
	public Response cadastraFilme(String filmeJson, @HeaderParam("token") long token);
	public Response excluiFilme(@PathParam("id") int id, @HeaderParam("token") long token);
	public Response modificaFilme(String filmeJson, @HeaderParam("token") long token);
	
}
