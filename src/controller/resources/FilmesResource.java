package controller.resources;

import java.util.List;

import javax.ws.rs.Path;

import com.google.gson.Gson;

import model.Filme;

@Path("/filmes")
public class FilmesResource implements IFilmesResources {

	private String listaFilmesToJson(List<Filme> filmes) {
		Gson gson = new Gson();
		String listaJson = gson.toJson(filmes);
		return listaJson;
	}

	private boolean validaToken(long token) {
		if (token == 1234) {
			return true;
		} else {
			return false;
		}
	}

}