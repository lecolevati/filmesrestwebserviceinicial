package controller.ws;

import org.glassfish.jersey.server.ResourceConfig;

public class InitWebService extends ResourceConfig {

	public InitWebService() {
		packages("controller.resources");
	}
	
}
